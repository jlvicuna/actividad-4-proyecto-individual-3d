using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target; // El objetivo que la cámara seguirá (tu carro).
    public Vector3 offset; // El desplazamiento relativo entre la cámara y el carro.

    void LateUpdate()
    {
        // Actualiza la posición de la cámara para que siga al carro,
        // manteniendo el desplazamiento especificado.
        transform.position = target.position + target.TransformDirection(offset);

        // Hace que la cámara mire siempre hacia el carro
        transform.LookAt(target);
    }
}
