using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Car : MonoBehaviour
{
    /*
    public float velocidadHorizontal = 5f; // Velocidad de movimiento horizontal del jugador
    public float velocidadAdelante = 10f; // Velocidad de movimiento hacia adelante del jugador
    public float velocidadGiro = 100f; // Velocidad de giro del vehículo
    */
    public float velocidadAdelante = 5f;
    public float velocidadHorizontal = 2f;
    public float aceleracionGiro = 30f;
    private float anguloGiro = 0f;

    public int vidaMaxima = 100;
    private int vidaActual;




    void Start()
    {
        vidaActual = vidaMaxima; // Inicializa la vida actual
        //barraDeVida.maxValue = vidaMaxima; // Establece el valor máximo de la barra de vida
        //barraDeVida.value = vidaActual; // Inicializa el valor de la barra de vida
    }


    void Update()
    {

        // Movimiento hacia adelante automático
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.forward * Time.deltaTime * velocidadAdelante);
        }
        // Giro progresivo del vehículo
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            anguloGiro -= aceleracionGiro * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            anguloGiro += aceleracionGiro * Time.deltaTime;
        }
        else
        {
            anguloGiro = 0; // Resetea el giro cuando no se presiona A o D
        }
        
        transform.Rotate(Vector3.up, anguloGiro * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que entra en el trigger tiene el tag 'garbageTag'
        if (other.CompareTag("garbageTag"))
        {
            PerderVida(10); // Llama a la función PerderVida con la cantidad de vida a perder por colisión
        }
    }


    private void PerderVida(int cantidad)
    {
        vidaActual -= cantidad; // Reduce la vida actual
        //barraDeVida.value = vidaActual; // Actualiza la barra de vida

        // Comprueba si la vida ha llegado a 0
        if (vidaActual <= 0)
        {
            Debug.Log("El jugador se quedó sin vida");
            // Recarga la escena actual
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
